# UE4-Playground
## Week of Mar 03 to Mar 09
This is my first week of playing around seriously with the Unreal Engine.
I will be keep a very meticulous check list of everything I want to get done for the week in terms of what it is 
I would like to learn. I will be utilizing YouTube video tutorials to learn as much about the engine that I can.
My goal is to have at least 1 working personal project before the summer time starts. 

Because I do not have my own computer at the time of writing this, I will be utilizing laboratory computers on
my college campus in order to promote my productivity. 

### Action Items for this week:
- [X] Blueprint Basics
    - [X] What are actors?
    
## Update Apr 30
I have done more or less an extensive amount of stuff inside the Unreal Engine. I have created a first person
character. I have learned how to add components to the characters to make them fairly customized. I managed to
create my own health system (although it only functions halfway). I want to turn the example project into something
that would demonstrate that I know what it takes to be a game developer.

### Action Items
- [ ] Get enemies working
    - [ ] Enemies damage player
    - [ ] Player damages enemies